﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {

	public static Water _instance;

	[Header("Bad touch")]
	public MeshRenderer _meshRenderer;
	
	private void Awake() {
		_instance = this;
		_meshRenderer = GetComponent<MeshRenderer>();
	}
}
