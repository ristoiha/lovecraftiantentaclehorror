﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static UIManager _instance;

	public Image _overlayBlackScreen;
	private float _overlayFadeTime = 0.5f;
	//private Color _colorBlack = Color.black;
	//private Color _colorClear = Color.clear;

	[Header("Canvases")]
	public Canvas _blackScreenCanvas;
	public Canvas _menuCanvas;
	public Canvas _gameOverCanvas;
	public Canvas _switchCanvas;
	public Canvas _hudCanvas;
	public Canvas _helpCanvas;
	public Canvas _creditsCanvas;
	public Canvas _settingsCanvas;
	public Canvas _licensesCanvas;

	[Header("Menu stuff")]
	public Button[] _buttons;
	public RectTransform _switchHandle;
	public AnimationCurve _switchAnimationCurve;
	public ParticleSystem _switchElectricParticles1;
	public ParticleSystem _switchElectricParticles2;
	public AnimationCurve _smoothFadeCurve;
	public Text _gameOverScore;
	public Text _gameOverTopScore;
	private Vector3[] _switchRotations = { new Vector3(0f, 0f, 45f), new Vector3(0f, 0f, -45f) };
	public AudioSource _uiAudioSource;
	public AudioClip _switchSound;
	public AudioClip _switchElectricitySound;
	public Slider _volumeSlider;

	[Header("Settings stuff")]
	public Text _postProcessingButtonText;
	public Text _shadowQualityButtonText;
	public Text _waterQualityButtonText;

	[Header("HUD stuff")]
	public Image _bloodOverlay;
	public Sprite[] _icons;
	public RectTransform _popUpPrefab;
	public Text _levelTimerText;
	public Text _scoreText;

	public Image _hypnosisBarFill;
	private Image _hypnosisBarParent;
	public Gradient _hypnosisBarGradient;
	public Transform _healthIconParent;
	public AudioClip _lowTimeSound;

	[Header("Info stuff")]
	public Sprite[] _infoSprites;
	public Image _infoImage;
	public Text[] _infoTexts;
	public Transform _switchHelp;
	public Button[] _infoButtons;

	private Animator _timerTextAnimator;
	private Coroutine _scoreTextRoutine;
	private Coroutine _timeTextRoutine;
	private int _scoreValue;
	private int _timeValue;
	private RectTransform _scoreRect;
	private RectTransform _timeRect;
	private int _previousLowTime = 9999;

	private Coroutine _bloodOverlayRoutine;

	private string[] _infoStrings = new string[] {
		"The Great One hungers.\nUse the <color=#a52a2aff>LIGHT</color> to lure unsuspecting ships into one of His many <color=#a52a2aff>MAWS</color>.",
		"You will be blessed with <color=#a52a2aff>BLOOD</color> and <color=#a52a2aff>TIME</color>.",
		"Hold down <color=#a52a2aff>MOUSE 1</color> to reach ships further away and <color=#a52a2aff>HYPNOTIZE</color> them.",
		"You may also <color=#a52a2aff>WRECK</color> them upon rocks or each other.",
		"Devouring wrecked ships will only grant you <color=#a52a2aff>BLOOD</color>.",
		"Hold down <color=#a52a2aff>SHIFT</color> to speed through the waters."
	};
	private string[] _infoStrings2 = new string[] {
		"The Great One hungers.\nUse the LIGHT to lure unsuspecting ships into one of His many MAWS.",
		"You will be blessed with BLOOD and TIME.",
		"Hold down MOUSE 1 to reach ships further away and HYPNOTIZE them.",
		"You may also WRECK them upon rocks or each other.",
		"Devouring wrecked ships will only grant you BLOOD.",
		"Hold down SHIFT to speed through the waters."
	};
	private int _infoIndex = 0;

	void Awake() {
		_instance = this;
		_hypnosisBarParent = _hypnosisBarFill.transform.parent.GetComponent<Image>();
		_timerTextAnimator = _levelTimerText.GetComponent<Animator>();
	}

	public void StartManager() {
		_uiAudioSource.ignoreListenerPause = true;
		_switchHandle.localEulerAngles = _switchRotations[0];
		ShowMenu(true, false);
	}

	public IEnumerator FadeInScreen() {
		_overlayBlackScreen.transform.parent.gameObject.SetActive(true);
		_overlayBlackScreen.color = Color.black;
		for (float i = 0f; i < _overlayFadeTime; i += Time.unscaledDeltaTime) {
			float lerp = _smoothFadeCurve.Evaluate(i / _overlayFadeTime);
			_overlayBlackScreen.color = Color.Lerp(Color.black, Color.clear, lerp);
			yield return null;
		}
		_overlayBlackScreen.color = Color.clear;
		_overlayBlackScreen.transform.parent.gameObject.SetActive(false);
	}

	public IEnumerator FadeOutScreen() {
		_overlayBlackScreen.transform.parent.gameObject.SetActive(true);
		_overlayBlackScreen.color = Color.clear;
		for (float i = 0f; i < _overlayFadeTime; i += Time.unscaledDeltaTime) {
			float lerp = _smoothFadeCurve.Evaluate(i / _overlayFadeTime);
			_overlayBlackScreen.color = Color.Lerp(Color.clear, Color.black, lerp);
			yield return null;
		}
		_overlayBlackScreen.color = Color.black;
	}

	public void UpdateLevelTimer(float time) {
		_levelTimerText.text = Mathf.Floor(time / 60) + ":" + Mathf.Floor(time % 60).ToString("00");
		int timeLeft = Mathf.FloorToInt(time);
		if (timeLeft < 11 && timeLeft % 2 == 0) {
			if (timeLeft < _previousLowTime) {
				_previousLowTime = timeLeft;
				_timerTextAnimator.Play("Low");
				CameraManager._instance._effectAudioSource.PlayOneShot(_lowTimeSound);
				ShowBloodOverlay((11 - timeLeft) / 11f);
			}
		}
	}

	public void UpdateScore(int score) {
		_scoreText.text = score.ToString();
	}

	public void UpdateHypnotizeBar(float fillPercentage) {
		_hypnosisBarFill.fillAmount = fillPercentage;
		_hypnosisBarFill.color = _hypnosisBarGradient.Evaluate(fillPercentage);
		if (fillPercentage == 0f) {
			_hypnosisBarParent.color = Color.gray;
		}
		else {
			_hypnosisBarParent.color = Color.white;
		}
	}

	public void UpdateHealth(int health) {
		for (int i = _healthIconParent.childCount - 1; i >= 0; i--) {
			if (health <= i) {
				_healthIconParent.GetChild(i).gameObject.SetActive(false);
			}
		}
	}

	public void ShowHud(bool b) {
		_hudCanvas.gameObject.SetActive(b);
	}

	public void ShowMenu(bool b, bool withBlackScreen) {
		EnableButtons(true);
		_switchCanvas.gameObject.SetActive(b);
		_menuCanvas.gameObject.SetActive(b);
		if (withBlackScreen == true) {
			_blackScreenCanvas.gameObject.SetActive(b);
		}
		// Quick fix for settings etc
		if (b == false) {
			EventSystem.current.SetSelectedGameObject(null);
			_helpCanvas.gameObject.SetActive(b);
			_creditsCanvas.gameObject.SetActive(b);
			_settingsCanvas.gameObject.SetActive(b);
			_licensesCanvas.gameObject.SetActive(b);
		}
	}

	public void ShowGameOver(bool b) {
		_switchCanvas.gameObject.SetActive(b);
		_gameOverCanvas.gameObject.SetActive(b);
		EnableButtons(true);
		_gameOverScore.text = LevelManager._instance._score.ToString();
		_gameOverTopScore.text = GameManager._instance._topScore.ToString();
	}

	private void EnableButtons(bool b) {
		for (int i = 0; i < _buttons.Length; i++) {
			_buttons[i].gameObject.SetActive(b);
		}
	}

	private IEnumerator PowerSwitchRoutine() {
		GameManager._instance.ChangeCursorLock();
		EnableButtons(false);
		Vector3 startRot;
		Vector3 endRot;
		float animationTime = 1f;

		if (GameManager._instance._gameState == GameManager.GameState.MainMenu) {
			startRot = _switchRotations[0];
			endRot = _switchRotations[1];
		}
		else {
			startRot = _switchRotations[1];
			endRot = _switchRotations[0];
		}
		// Animate switch
		_uiAudioSource.PlayOneShot(_switchSound);
		if (GameManager._instance._gameState == GameManager.GameState.MainMenu) {
			_switchElectricParticles1.Play();
		}
		for (float i = 0f; i < animationTime; i += Time.unscaledDeltaTime) {
			float lerp = _switchAnimationCurve.Evaluate(i / animationTime);
			_switchHandle.localEulerAngles = Vector3.Lerp(startRot, endRot, lerp);
			if (i / animationTime >= 0.85f && _switchElectricParticles2.isPlaying == false) {
				// Aciavate lighthouse
				if (GameManager._instance._gameState == GameManager.GameState.MainMenu) {
					_uiAudioSource.PlayOneShot(_switchElectricitySound);
					_switchElectricParticles2.Play();
					StartCoroutine(Player._instance.CinematicActivateLightRoutine(true));
				}
				else {
					StartCoroutine(Player._instance.CinematicActivateLightRoutine(false));
				}
			}
			yield return null;
		}
		// SOUND HERE
		_switchHandle.localEulerAngles = endRot;
		yield return new WaitForSecondsRealtime(1f);

		if (GameManager._instance._gameState == GameManager.GameState.MainMenu) {
			ShowMenu(false, false);
			yield return StartCoroutine(CameraOrbit._instance.GameStartAnimationRoutine());
			ShowHud(true);
			GameManager._instance.StartGame();
		}
		else {
			GameManager._instance.RestartGame();
		}
	}

	public void CreatePopUp(Vector3 worldPos, int value, bool time) {
		if (time == false) {
			if (_scoreTextRoutine != null) {
				StopCoroutine(_scoreTextRoutine);
				Destroy(_scoreRect.gameObject);
			}
			_scoreValue += value;
			_scoreTextRoutine = StartCoroutine(PopUpRoutine(worldPos, _scoreValue, time));
		}
		else {
			if (_timeTextRoutine != null) {
				StopCoroutine(_timeTextRoutine);
				Destroy(_timeRect.gameObject);
			}
			_timeValue += value;
			_timeTextRoutine = StartCoroutine(PopUpRoutine(worldPos, _timeValue, time));
		}
	}

	private IEnumerator PopUpRoutine(Vector3 worldPos, int value, bool time) {
		float popupLifetime = 3f;
		Vector3 pos;
		RectTransform popup = null;
		if (time == true) {
			pos = _levelTimerText.transform.position + Vector3.up;
			popup = Instantiate(_popUpPrefab, pos, Quaternion.identity, _hudCanvas.transform);
			_timeRect = popup;
		}
		else {
			pos = _scoreText.transform.position + Vector3.up;
			popup = Instantiate(_popUpPrefab, pos, Quaternion.identity, _hudCanvas.transform);
			_scoreRect = popup;
		}
		int spriteIndex = 0;
		if (time == true) {
			spriteIndex = 1;
		}
		Image icon = popup.GetChild(0).GetComponent<Image>();
		Text numbers = popup.GetChild(1).GetComponent<Text>();
		icon.sprite = _icons[spriteIndex];
		numbers.text = "+" + value.ToString();

		Vector3 startPos = pos;
		Vector3 endPos = pos + Vector3.up * 2f;
		for (float i = 0f; i < popupLifetime; i += Time.deltaTime) {
			float lerp = _smoothFadeCurve.Evaluate(i / popupLifetime);
			popup.position = Vector3.Lerp(startPos, endPos, lerp);
			if (lerp > 0.5f) {
				Color lerpColor = Color.Lerp(Color.white, new Color(1f, 1f, 1f, 0f), (lerp - 0.5f) / 0.5f);
				icon.color = lerpColor;
				numbers.color = lerpColor;
			}
			yield return null;
		}

		if (time == false) {
			_scoreValue = 0;
			_scoreTextRoutine = null;
		}
		else {
			_timeValue = 0;
			_timeTextRoutine = null;
		}
		Destroy(popup.gameObject);
	}

	public void ShowBloodOverlay(float alpha) {
		if (_bloodOverlayRoutine != null) {
			StopCoroutine(_bloodOverlayRoutine);
		}
		_bloodOverlayRoutine = StartCoroutine(BloodOverlayRoutine(alpha));
	}

	private IEnumerator BloodOverlayRoutine(float alpha) {
		for (float i = 1f; i > 0f; i -= Time.deltaTime) {
			_bloodOverlay.color = new Color(1f, 1f, 1f, i * alpha);
			yield return null;
		}
		_bloodOverlay.color = new Color(1f, 1f, 1f, 0f);
	}

	private void ShowInfoScreen() {
		if (_infoIndex == 0) {
			_switchHelp.gameObject.SetActive(true);
			_infoButtons[0].gameObject.SetActive(false);
		}
		else {
			_switchHelp.gameObject.SetActive(false);
			_infoButtons[0].gameObject.SetActive(true);
		}
		if (_infoIndex < _infoStrings.Length - 1) {
			_infoButtons[1].gameObject.SetActive(true);
		}
		else {
			_infoButtons[1].gameObject.SetActive(false);
		}
		_infoImage.sprite = _infoSprites[_infoIndex];
		_infoTexts[0].text = _infoStrings2[_infoIndex];
		_infoTexts[1].text = _infoStrings[_infoIndex];
	}

	public void ResetSettingsTexts(string pp, string sq, string wq, float v) {
		_postProcessingButtonText.text = pp;
		_shadowQualityButtonText.text = sq;
		_waterQualityButtonText.text = wq;
		_volumeSlider.value = v;
	}


	// ---
	// EDITOR COMMANDS
	// ---


	public void UI_PoweSwitch() {
		EventSystem.current.SetSelectedGameObject(null);
		StartCoroutine(PowerSwitchRoutine());
	}

	public void UI_ShowHelp(bool b) {
		EventSystem.current.SetSelectedGameObject(null);
		_infoIndex = 0;
		_helpCanvas.gameObject.SetActive(b);
		_menuCanvas.gameObject.SetActive(!b);
		ShowInfoScreen();
	}

	public void UI_ShowCredits(bool b) {
		EventSystem.current.SetSelectedGameObject(null);
		_creditsCanvas.gameObject.SetActive(b);
		_menuCanvas.gameObject.SetActive(!b);
	}

	public void UI_ShowSettings(bool b) {
		EventSystem.current.SetSelectedGameObject(null);
		_settingsCanvas.gameObject.SetActive(b);
		_menuCanvas.gameObject.SetActive(!b);
	}

	public void UI_ShowLicenses(bool b) {
		EventSystem.current.SetSelectedGameObject(null);
		_licensesCanvas.gameObject.SetActive(b);
		_creditsCanvas.gameObject.SetActive(!b);
	}

	public void UI_NextInfo(bool b) {
		if (b == true) {
			_infoIndex++;
			if (_infoIndex == _infoStrings.Length) {
				_infoIndex = 0;
			}
		}
		else {
			_infoIndex--;
			if (_infoIndex < 0) {
				_infoIndex = _infoStrings.Length - 1;
			}
		}
		ShowInfoScreen();
	}

	public void UI_ChangePostProcessing() {
		_postProcessingButtonText.text = GameManager._instance.ChangePostProcessing();
	}

	public void UI_ChangeShadowQuality() {
		_shadowQualityButtonText.text = GameManager._instance.ChangeShadowQuality();
	}

	public void UI_ChangeWaterQuality() {
		_waterQualityButtonText.text = GameManager._instance.ChangeWaterQuality();
	}

	public void UI_ChangeVolume() {
		GameManager._instance.ChangeVolume(_volumeSlider.value);
	}

	public void UI_QuitGame() {
		Application.Quit();
	}
}
