﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rock : MonoBehaviour {

	public Transform[] _models;
	
	void Awake() {
		for (int i = 0; i < transform.GetChild(0).childCount; i++) {
			transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
		}
		Transform rock = transform.GetChild(0).GetChild(Random.Range(0, _models.Length));
		rock.gameObject.SetActive(true);
		rock.localScale = Vector3.one * Random.Range(1f, 1.5f);
		transform.Rotate(Vector3.up, Random.Range(0f, 360f));

	}
}
