﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class ShipAI : MonoBehaviour {

	public enum NavigationState { Idle, StartPathing, Pathing, EndPathing }
	public enum LifeState { Alive, Wrecked, Destroyed }
	public enum ShipSize { Small, Large }

	public int scoreValue = 100;
	public int timeValue = 10;

	public float _moveSpeed = 3f;
	public float _turningSpeed = 20f;
	public ShipSize _shipSize;
	public float _hypnotizingTime = 1.8f;
	public float _hypnosisDuration = 5f;
	public AudioClip _damageSound;
	public AudioClip _bellSound;
	public AudioClip[] _fireSounds;
	private float _hypnosisBoostMultp = 1.5f;
	public ParticleSystem[] _waterParticles;
	public Transform _wreckParticlesPrefab;
	public AudioSource _bellAudioSource;
	public AudioSource _damageAudioSource;

	[Header("No touch!")]
	public NavigationState _navigationState = NavigationState.Idle;
	public LifeState _lifeState = LifeState.Alive;

	private Rigidbody _rigidbody;
	private Transform _rotationParent;
	private Transform _wreck;
	private ParticleSystem _beamParticles;
	private ParticleSystem _hypnosisParticles;
	private NavMeshAgent _navMeshAgent;
	private Vector3 _currentDestination;
	private Vector3 _finalDestination;

	private NavMeshQueryFilter _pathFilter;
	private Vector3[] _activePath;
	private int _activePathNodeIndex = 0;
	private float _maxNodeDistance = 1f;

	private bool _gettingHypnotized = false;
	private float _hypnotizingTimeTimer = 0f;
	private float _hypnosisDurationTimer = 0f;
	private bool _inLight = false;
	private float _inLightRepathInterval = 0.1f;
	private float _inLightRepathTimer = 0;

	private float _bellRingTime;
	private float _bellRingTimeTimer = 0f;
	private float _bellVolume;
	private float _bellRaycastSize;
	private float _bellRaycastTime = 0.5f;
	private float _bellRaycastTimeTimer = 0f;

	private float _waterLoopInterval = 0.4f;
	private float _waterLoopTimer = 0f;

	private float _wreckTimer = 15f;

	void Awake() {
		_bellAudioSource = GetComponent<AudioSource>();
		_rigidbody = GetComponent<Rigidbody>();
		_rotationParent = transform.GetChild(0);
		_wreck = transform.GetChild(0).GetChild(1);
		_beamParticles = transform.GetChild(0).GetChild(2).GetComponent<ParticleSystem>();

		if (_shipSize == ShipSize.Small) {
			_bellRaycastSize = 5f;
			_bellRingTime = 0.5f;
		}
		else {
			_bellRaycastSize = 10f;
			_bellRingTime = 0.7f;
		}
		_bellVolume = 0.8f;
		_bellAudioSource.maxDistance = _bellRaycastSize * 2f * 1.2f;

		_hypnosisParticles = transform.GetChild(0).GetChild(3).GetComponent<ParticleSystem>();
		_navMeshAgent = GetComponent<NavMeshAgent>();
		_pathFilter.agentTypeID = 0;
		_pathFilter.areaMask = -1;
		_navMeshAgent.enabled = false;
		StartNavigation();
	}

	private void StartNavigation() {
		_rotationParent.LookAt(Vector3.zero);
		_rotationParent.Rotate(Vector3.up, Random.Range(-45f, 45f));
		FindPath(_rotationParent.forward * 150f, true);
	}

	public void FindPath(Vector3 target, bool finalDestination = false) {
		//print(Time.frameCount + " FINDING PATH");
		NavMeshPath path = new NavMeshPath();
		bool success = NavMesh.CalculatePath(_rotationParent.position, target, _pathFilter, path);
		if (success == true) {
			_navigationState = NavigationState.Pathing;
			_activePathNodeIndex = 0;
			_activePath = new Vector3[path.corners.Length];
			//print(path.corners.Length);
			for (int i = 0; i < path.corners.Length; i++) {
				_activePath[i] = path.corners[i];
				_activePath[i].y = 0f;
			}
			if (finalDestination == true) {
				_finalDestination = _activePath[_activePath.Length - 1];
			}
		}
	}

	public void SetStraightPath(Vector3 destination) {
		//print(Time.frameCount + " STRIGHT GOING");
		_activePathNodeIndex = 1;
		//print(destination);
		destination.y = 0f;
		_activePath = new Vector3[] { _rotationParent.position, destination };
	}

	private void Update() {
		if (GameManager._instance._gameState == GameManager.GameState.Running) {
			if (_lifeState == LifeState.Alive) {
				_bellRingTimeTimer -= Time.deltaTime;
				_bellRaycastTimeTimer -= Time.deltaTime;
				if (Vector3.Distance(_rotationParent.position, _finalDestination) < _maxNodeDistance) {
					DespawnShip();
					return;
				}
				if (_inLight == true) {
					_inLightRepathTimer -= Time.deltaTime;
					if (_inLightRepathTimer <= 0f) {
						//print(Time.frameCount + " Path -> Player");
						SetStraightPath(Player._instance.transform.position);
						_inLightRepathTimer = _inLightRepathInterval;
					}

					// Set hypnosis
					if (Player._instance._hypnosisActive == true) {
						_gettingHypnotized = true;
						if (_hypnotizingTimeTimer < 0f) {
							_hypnotizingTimeTimer = 0f;
						}
						_hypnotizingTimeTimer += Time.deltaTime;
						if (_hypnotizingTimeTimer >= _hypnotizingTime) {
							_hypnosisDurationTimer = _hypnosisDuration;
							if (_hypnosisParticles.isPlaying == false) {
								_hypnosisParticles.Play();
							}
						}
					}
					else {
						ProcessHypnosis();
					}
				}
				else {
					ProcessHypnosis();
					// Bell
					if (_hypnosisDurationTimer <= 0f) {
						if (_bellRaycastTimeTimer <= 0f) {
							_bellRaycastTimeTimer = _bellRaycastTime;
							if (_bellRingTimeTimer <= 0f) {

								Vector3 playerDir = (Player._instance.transform.position - transform.position).normalized;
								playerDir.y = 0f;
								float dot = Vector3.Dot(_rotationParent.forward, playerDir);
								if (dot < 0f) {
									dot = 0f;
								}
								float multp = 1f + dot;

								Debug.DrawRay(transform.position, playerDir * _bellRaycastSize * multp, Color.white);
								if (Physics.Raycast(transform.position, playerDir, _bellRaycastSize * multp, LevelManager._instance._playerMask) == true) {
									_bellRingTimeTimer = _bellRingTime;
									_bellAudioSource.PlayOneShot(_bellSound, _bellVolume);
								}
							}
						}
					}
				}
			}
			else if (_lifeState == LifeState.Wrecked) {
				_wreckTimer -= Time.deltaTime;
				_waterLoopTimer -= Time.deltaTime;
				if (_waterLoopTimer <= 0f) {
					_waterLoopTimer = _waterLoopInterval;
					_bellAudioSource.PlayOneShot(_fireSounds[Random.Range(0, _fireSounds.Length)], 0.5f);

				}
				if (_wreckTimer <= 0f) {
					StartCoroutine(SinkRoutine());
				}
			}
		}
	}

	private void ProcessHypnosis() {
		_gettingHypnotized = false;
		_hypnotizingTimeTimer -= Time.deltaTime;
		if (_hypnosisDurationTimer > 0f) {
			_hypnosisDurationTimer -= Time.deltaTime;
			//print("Hyping: " + _hypnosisDurationTimer);
			if (_hypnosisDurationTimer >= 0f) {
				if (_inLight == false) {
					SetStraightPath(_rotationParent.position + _rotationParent.forward * 10f);
				}
			}
			else {
				_hypnosisParticles.Stop();
				FindPath(_finalDestination);
			}
		}
		else {
			if (_inLight == false && _activePath[_activePath.Length - 1] != _finalDestination) {
				FindPath(_finalDestination);
			}
		}
	}

	void FixedUpdate() {
		if (GameManager._instance._gameState == GameManager.GameState.Running) {
			if (_lifeState == LifeState.Alive) {
				if (_navigationState == NavigationState.Pathing) {
					//print("going to " + _activePath[_activePathNodeIndex]);
					float tSpeed = _turningSpeed * Mathf.Deg2Rad * Time.deltaTime;
					float mSpeed = _moveSpeed * Time.fixedDeltaTime;
					if (_gettingHypnotized == true || _hypnosisDurationTimer > 0f) {
						tSpeed *= _hypnosisBoostMultp;
						mSpeed *= _hypnosisBoostMultp;
					}
					// Rotate ship
					Vector3 lookVector = Vector3.RotateTowards(_rotationParent.forward, _activePath[_activePathNodeIndex] - _rotationParent.position, tSpeed, 0f);
					_rotationParent.rotation = Quaternion.LookRotation(lookVector);
					// Move ship
					transform.Translate(_rotationParent.forward * mSpeed);

					// Navigation stuff
					if (_navigationState == NavigationState.Pathing && Vector3.Distance(_rotationParent.position, _activePath[_activePathNodeIndex]) < _maxNodeDistance) {
						if (_activePathNodeIndex < _activePath.Length - 1) {
							_activePathNodeIndex++;
						}
						else {
							_navigationState = NavigationState.EndPathing;
						}
					}
				}
			}
		}
		_rigidbody.velocity = Vector3.zero;
		_rigidbody.rotation = Quaternion.identity;
	}

	private void OnCollisionEnter(Collision collision) {
		if (collision.collider.tag == "Ship") {
			// Crashing
			ShipAI otherShip = collision.collider.transform.parent.GetComponent<ShipAI>();
			if (_shipSize <= otherShip._shipSize) {
				GetWrecked();
			}
		}
		if (collision.collider.tag == "Obstacle") {
			GetWrecked();
		}
		if (collision.collider.tag == "Player") {
			if (_shipSize == ShipSize.Small) {
				if (_lifeState == LifeState.Alive) {
					GetWrecked();
					Player._instance.TakeDamage(1, transform.position);
				}
				else {
					Transform particles = Instantiate(_wreckParticlesPrefab, transform.position, Quaternion.identity);
					particles.GetChild(0).GetChild(0).gameObject.SetActive(false);
					_damageAudioSource.PlayOneShot(_damageSound, 1f);
					StartCoroutine(DestroyRoutine());
				}
			}
			else {
				Player._instance.TakeDamage(1, transform.position);
			}
		}
	}

	private void OnTriggerStay(Collider other) {
		if (other.tag == "Lightbeam") {
			if (_lifeState == LifeState.Alive) {
				if (other.transform.parent.GetComponent<MeshRenderer>().enabled == true) {
					if (_inLight == false) {
						_inLight = true;
						_beamParticles.Play();
					}
				}
				else {
					_inLight = false;
					_beamParticles.Stop();
				}
			}
		}
	}

	private void OnTriggerExit(Collider other) {
		_inLight = false;
		_beamParticles.Stop();
	}

	public void GetEaten(bool increaseTime) {
		Instantiate(_wreckParticlesPrefab, transform.position, Quaternion.identity);

		if (increaseTime == true) {
			LevelManager._instance.IncreaseValues(scoreValue, timeValue, transform.position);
		}
		else {
			LevelManager._instance.IncreaseValues(scoreValue, 0, transform.position);
		}
		StartCoroutine(DestroyRoutine());
	}

	private void GetWrecked() {
		Instantiate(_wreckParticlesPrefab, transform.position, Quaternion.identity);

		for (int i = 0; i < _waterParticles.Length; i++) {
			ParticleSystem.EmissionModule eMod = _waterParticles[i].emission;
			if (i == 0) {
				eMod.rateOverTime = new ParticleSystem.MinMaxCurve(1f);
			}
			else {
				eMod.rateOverTime = new ParticleSystem.MinMaxCurve(0f);
			}
		}
		_lifeState = LifeState.Wrecked;
		_beamParticles.Stop();
		_hypnosisParticles.Stop();
		_hypnosisDurationTimer = 0f;
		_rotationParent.GetChild(0).gameObject.SetActive(false);
		_wreck.gameObject.SetActive(true);
		_damageAudioSource.PlayOneShot(_damageSound, 1f);
	}

	private void DespawnShip() {
		Destroy(gameObject);
	}

	private IEnumerator DestroyRoutine() {
		_lifeState = LifeState.Destroyed;
		transform.GetChild(0).GetComponent<CapsuleCollider>().enabled = false;
		for (int i = 0; i < _rotationParent.childCount; i++) {
			_rotationParent.GetChild(i).gameObject.SetActive(false);
		}
		//for (int i = 0; i < _waterParticles.Length; i++) {
		//	ParticleSystem.EmissionModule eMod = _waterParticles[i].emission;
		//	eMod.rateOverTime = new ParticleSystem.MinMaxCurve(0f);
		//}

		yield return new WaitForSeconds(5f);
		Destroy(gameObject);
	}

	private IEnumerator SinkRoutine() {
		_lifeState = LifeState.Destroyed;
		transform.GetChild(0).GetComponent<CapsuleCollider>().enabled = false;
		_damageAudioSource.volume = 0f;
		transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<ParticleSystem>().Stop();
		transform.GetChild(0).GetChild(1).GetChild(1).GetComponent<ParticleSystem>().Stop();

		for (float i = 0f; i < 6f; i += Time.deltaTime) {
			transform.position += Vector3.down * 1f * Time.deltaTime;
			yield return null;
		}
		DespawnShip();
	}
}
