﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

	public float _destroyTime;
	private float _timer;
	
	private void Awake() {
		_timer = _destroyTime;
	}
	
	private void Update() {
		_timer -= Time.deltaTime;
		if (_timer <= 0f) {
			Destroy(gameObject);
		}
	}
}
