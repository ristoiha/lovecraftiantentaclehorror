﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public enum GameState { MainMenu, PauseMenu, Running, GameOver, Detatched }
	public static string[] _postProcessingTexts = new string[] { "Off", "On" };
	public static string[] _shadowQualityTexts = new string[] { "None", "Hard", "Soft" };
	public static string[] _waterQualityTexts = new string[] { "Low", "High" };
	public static GameManager _instance;
	
	public GameState _gameState = GameState.MainMenu;
	public Material[] _waterMaterials;
	public int _topScore = 0;

	private Light _directionalLight;
	private MeshRenderer _waterMeshRenderer;

	private bool _postProcessingOn = true;
	private int _shadowQuality = 2;
	private int _waterQuality = 1;
	private float _gameVolume = 1;

	private void Awake() {
		if (_instance == null) {
			_instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else {
			Destroy(gameObject);
		}
	}

	private void Start() {
		if (_instance == this) {
			CustomStart();
		}
	}

	private void OnLevelWasLoaded(int level) {
		if (_instance == this) {
			CustomStart();
		}
	}

	private void CustomStart() {
		_gameState = GameState.MainMenu;
		Time.timeScale = 1f;
		AudioListener.pause = false;
		_directionalLight = FindObjectOfType<Light>();
		_waterMeshRenderer = Water._instance._meshRenderer;
		LevelManager._instance.StartManager();
		CameraManager._instance.StartManager();
		UIManager._instance.StartManager();
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
		//DataManager._instance.UpdateGraphicsMode();
		InitializeGraphics();
		StartCoroutine(UIManager._instance.FadeInScreen());
	}

	private void Update() {
		#if UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.Tab)) {
				ChangeCursorLock();
			}
		#endif
		
		if ((_gameState == GameState.Running && Input.GetKeyDown(KeyCode.F1)) ||
			(_gameState == GameState.Detatched && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.F1)))) {
				CameraManager._instance.DetachCamera();
		}
		if (Player._instance._alive == true && (_gameState == GameState.Running || _gameState == GameState.PauseMenu)) {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				PauseGame();
			}
		}
	}

	public void ChangeCursorLock() {
		if (Cursor.lockState == CursorLockMode.Locked) {
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;
		}
		else {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		//print(Cursor.lockState + " - " + Time.frameCount);
	}

	public void StartGame() {
		_gameState = GameState.Running;
		LevelManager._instance.StartGame();
	}

	public void PauseGame() {
		if (_gameState == GameState.PauseMenu) {
			_gameState = GameState.Running;
			Time.timeScale = 1f;
			AudioListener.pause = false;
			UIManager._instance.ShowMenu(false, true);
		}
		else {
			if (CameraManager._instance._cameraDetached == true) {
				CameraManager._instance.DetachCamera();
			}
			_gameState = GameState.PauseMenu;
			Time.timeScale = 0f;
			AudioListener.pause = true;
			UIManager._instance.ShowMenu(true, true);
		}
		ChangeCursorLock();
	}

	public void RestartGame() {
		StartCoroutine(RestartRoutine());
	}

	private IEnumerator RestartRoutine() {
		yield return StartCoroutine(UIManager._instance.FadeOutScreen());
		yield return new WaitForSecondsRealtime(0.5f);
		SceneManager.LoadScene(0);
	}

	private void InitializeGraphics() {
		CameraManager._instance.SetPostProcessing(_postProcessingOn);
		_directionalLight.shadows = (LightShadows)_shadowQuality;
		_waterMeshRenderer.material = _waterMaterials[_waterQuality];
		Player._instance.ChangeBeamMaterial(_waterQuality);

		UIManager._instance.ResetSettingsTexts(
			_postProcessingTexts[System.Convert.ToInt32(_postProcessingOn)],
			_shadowQualityTexts[_shadowQuality],
			_waterQualityTexts[_waterQuality],
			_gameVolume);
	}

	public string ChangePostProcessing() {
		_postProcessingOn = !_postProcessingOn;
		CameraManager._instance.SetPostProcessing(_postProcessingOn);

		return _postProcessingTexts[System.Convert.ToInt32(_postProcessingOn)];
	}

	public string ChangeShadowQuality() {
		_shadowQuality--;
		if (_shadowQuality < 0) {
			_shadowQuality = 2;
		}
		_directionalLight.shadows = (LightShadows)_shadowQuality;
		return _shadowQualityTexts[_shadowQuality];
		// TODO: Force water quality if no shadows
	}

	public string ChangeWaterQuality() {
		_waterQuality--;
		if (_waterQuality < 0) {
			_waterQuality = 1;
		}
		_waterMeshRenderer.material = _waterMaterials[_waterQuality];
		Player._instance.ChangeBeamMaterial(_waterQuality);
		return _waterQualityTexts[_waterQuality];
	}

	public void ChangeVolume(float volume) {
		_gameVolume = volume;
		AudioListener.volume = volume;
	}
}
