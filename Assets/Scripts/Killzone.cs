﻿using System.Collections;
using UnityEngine;

public class Killzone : MonoBehaviour {

	public bool _playerKillzone = false;
	public Animator _mouthAnimator;
	public ParticleSystem _particles;
	public AudioClip _eatSound;
	public AudioClip[] _splashSounds;

	private AudioSource _audioSource;
	private Coroutine _eatingRoutine = null;
	
	void Awake() {
		_audioSource = GetComponent<AudioSource>();
	}

	private void OnTriggerStay(Collider collider) {
		if (_playerKillzone == true) {
			if (Player._instance._lightCooldownTimer > 0f) {
				return;
			}
		}
		if (collider.tag == "Ship") {
			if (_playerKillzone == true) {
				ShipAI ship = collider.transform.parent.GetComponent<ShipAI>();
				if (ship._lifeState != ShipAI.LifeState.Wrecked) {
					return;
				}
			}
			if (_eatingRoutine == null) {
				 _eatingRoutine = StartCoroutine(EatRoutine());
			}
		}
	}

	private IEnumerator EatRoutine() {
		if (_playerKillzone == true) {
			Player._instance.StartEating();
		}
		_mouthAnimator.transform.Rotate(Vector3.up, Random.Range(0f, 360f));
		Collider[] ships = Physics.OverlapSphere(transform.position, 6f, LevelManager._instance._shipMask);
		for (int i = 0; i < ships.Length; i++) {
			ships[i].transform.parent.GetComponent<ShipAI>().GetEaten(!_playerKillzone);
		}
		_audioSource.PlayOneShot(_eatSound, 0.75f);
		_audioSource.PlayOneShot(_splashSounds[Random.Range(0, _splashSounds.Length)], 0.75f);
		_particles.Play();
		_mouthAnimator.Play("MouthChomp");
		yield return null;
		AnimatorStateInfo info = _mouthAnimator.GetCurrentAnimatorStateInfo(0);
		yield return new WaitForSeconds(info.length);
		if (_playerKillzone == true) {
			Player._instance.EndEating();
		}
		_eatingRoutine = null;
	}
}
