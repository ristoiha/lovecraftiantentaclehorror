﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour {

	public static CameraOrbit _instance;

	public Transform _followTarget;
	//public float _targetDistance;
	public float _orbitSpeed;
	public AnimationCurve _startAnimationCurve;

	//public Vector3 _lookAngles;
	//public Vector3 _cameraOffset;

	public Transform _child;

	private Coroutine _orbitRoutine;

	private void Awake() {
		_instance = this;
		_child = transform.GetChild(0);
	}

	private void Start() {
		_orbitRoutine = StartCoroutine(OrbitRoutine());
	}

	private void StartOrbit() {

	}

	private void FixedUpdate() {
		if (_followTarget != null) {
			transform.position = _followTarget.position;
		}
	}

	//public void StartOrbit() {

	//}

	//public void StopOrbit() {

	//}

	private IEnumerator OrbitRoutine() {
		Vector3 cameraOffsetVector = CameraManager._instance._orbitFollowPositionOffset;
		transform.rotation = Quaternion.Euler(Vector3.zero);
		_child.localPosition = -Vector3.forward * cameraOffsetVector.z + -Vector3.up * cameraOffsetVector.y;
		yield return null;
		while (true) {
			transform.Rotate(Vector3.up, _orbitSpeed * Time.deltaTime);
			yield return null;
		}
	}

	public IEnumerator GameStartAnimationRoutine() {
		if (_orbitRoutine != null) {
			StopCoroutine(_orbitRoutine);
		}
		float animationTime = 1.5f;
		Quaternion startRot = transform.rotation;
		Vector3 startPos = _child.localPosition;
		Vector3 targetCameraPos = -Vector3.forward * CameraManager._instance._followPositionOffset.z + -Vector3.up * CameraManager._instance._followPositionOffset.y;
		for (float i = 0f; i < animationTime; i += Time.deltaTime) {
			float lerp = _startAnimationCurve.Evaluate(i / animationTime);
			transform.rotation = Quaternion.Slerp(startRot, Quaternion.LookRotation(Vector3.forward), lerp);
			_child.localPosition = Vector3.Lerp(startPos, targetCameraPos, lerp);
			CameraManager._instance._lerpLook = Vector3.Lerp(CameraManager._instance._orbitFollowLookOffset, CameraManager._instance._followLookOffset, lerp);
			CameraManager._instance._camera.fieldOfView = Mathf.Lerp(CameraManager._instance._animationFow, CameraManager._instance._gameFow, lerp);
			yield return null;
		}

		yield return null;
	}
}
