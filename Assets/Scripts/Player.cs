﻿using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour {

	public static Player _instance;

	public Material[] _beamMaterials;
	public Transform _beamModel;
	public AnimationCurve _submergeCurve;
	public AnimationCurve _deathSubmergeCurve;
	public AnimationCurve _deathShakeCurve;
	//public CapsuleCollider _killzoneCollider;
	public AudioClip _damageSound;
	public AudioClip _deathSound;
	public AudioClip[] _waterLoopLightSounds;
	public AudioClip[] _waterLoopHeavySounds;
	public AudioSource _lampAudioSource;
	public ParticleSystem[] _waterParticles;
	
	[Header("Hands off casuals")]
	public bool _hypnosisActive = false;
	public bool _alive = true;

	private AudioSource _effectAudioSource;
	private Rigidbody _rigidbody;
	private Transform _rotationParent;
	private Transform _model;
	private Vector3 _movementVector;
	private float _rotationSpeed = 40f;
	private float _rotationInput;
	private float _movementSpeed = 5f;
	private float _targetRotation = 0f;
	private bool _controlsEnabled = true;
	private float _waterLoopInterval = 0.4f;
	private float _waterLoopTimer = 0f;

	private int _maxHealth = 3;
	private int _health;
	private float _deathSinkTime = 5f;
	private float _deathSinkTimeTimer = 0f;

	private float _acceleration = 0.05f;
	private float _deceleration = 0.02f;
	private float _boostSpeedMultp = 2.5f;

	private float _lightCooldown = 1f;
	[HideInInspector]
	public float _lightCooldownTimer = 0f;

	private float _submergeTime = 0.75f;
	private float _submergeTimeTimer = 0f;

	private bool _eating = false;
	private float _eatTime = 0.5f;
	private float _eatTimeTimer = 0f;

	private float _hypnosisGasMax = 1f;
	private float _hypnosisGas;
	private float _hypnosisGasConsumption = 0.3f;
	private float _hypnosisGasRecharge = 0.15f;
	private float _hypnosisRechargeCooldown = 1f;
	private float _hypnosisRechargeCooldownTimer = 0f;

	private Material _beamMaterial;
	private ParticleSystem _beamParticle;
	private Vector3 _lightSizeDefault = new Vector3(2f, 2f, 2f);
	private Vector3 _lightSizeHypnosis = new Vector3(0.75f, 2f, 2f);
	private Color _lightColorDefault = new Color(0.5f, 0.5f, 0.5f, 0.0313f);
	private Color _lightColorHypnosis = new Color(0.5f, 0.5f, 0.5f, 0.0313f);
	//private Color _lightColorHypnosis = new Color(0.9f, 0.1f, 0.5f, 0.0313f);
	private Vector3 _lightRotationDefault = new Vector3(32f, 0f, 0f);
	private Vector3 _lightRotationHypnosis = new Vector3(26f, 0f, 0f);
	private float _lightLerpTime = 0.2f;
	private float _lightLerpTimeTimer = 0f;

	private float _modelRotationDampTime = 0.5f;
	private float _modelRotationVelocity = 0f;

	private float[] _waterParticleMaxEmission;

	private Coroutine _shakeRoutine;

	void Awake() {
		_instance = this;

		_rigidbody = GetComponent<Rigidbody>();
		_health = _maxHealth;
		_rotationParent = transform.GetChild(0);
		_model = transform.GetChild(1);
		_hypnosisGas = _hypnosisGasMax;
		_beamMaterial = _beamModel.GetComponent<MeshRenderer>().sharedMaterial;
		_beamParticle = _beamModel.parent.parent.GetChild(1).GetComponent<ParticleSystem>();

		_beamModel.GetComponent<MeshRenderer>().enabled = false;
		_beamParticle.Stop();

		_waterParticleMaxEmission = new float[_waterParticles.Length];
		for (int i = 0; i < _waterParticles.Length; i++) {
			ParticleSystem.EmissionModule eMod = _waterParticles[i].emission;
			_waterParticleMaxEmission[i] = eMod.rateOverTime.constantMax;
			if (i == 0) {
				eMod.rateOverTime = new ParticleSystem.MinMaxCurve(1f);
			}
			else {
				eMod.rateOverTime = new ParticleSystem.MinMaxCurve(0f);
			}
		}
	}

	private void Start() {
		_effectAudioSource = CameraManager._instance._effectAudioSource;
		UIManager._instance.UpdateHealth(_health);
	}

	void Update() {
		if (GameManager._instance._gameState == GameManager.GameState.Running) {
			if (_alive == true) {
				//if (Input.GetKeyDown(KeyCode.Alpha1)) {
				//	TakeDamage(1, Vector3.zero);
				//}

				Vector3 inputVectorNormalized = Vector3.zero;
				_rotationInput = 0f;
				bool boosting = false;
				bool hypnotizing = false;
				// Get player inputs
				if (_controlsEnabled == true) {
					_rotationInput = Input.GetAxis("Mouse X");
					_targetRotation += _rotationInput * _rotationSpeed * Time.deltaTime;
					inputVectorNormalized = (_rotationParent.right * Input.GetAxisRaw("Horizontal") + (_rotationParent.forward * Input.GetAxisRaw("Vertical"))).normalized;
					if (Input.GetKey(KeyCode.LeftShift)) {
						boosting = true;
						inputVectorNormalized *= _boostSpeedMultp;
					}
					if (Input.GetMouseButton(0)) {
						hypnotizing = true;
					}
				}
				// Set movement vector
				if (inputVectorNormalized.sqrMagnitude != 0) {
					_movementVector = Vector3.MoveTowards(_movementVector, inputVectorNormalized, _acceleration);
				}
				else {
					_movementVector = Vector3.MoveTowards(_movementVector, inputVectorNormalized, _deceleration);
				}
				// Water loop audio
				_waterLoopTimer -= Time.deltaTime;
				if (_waterLoopTimer <= 0f && _movementVector.sqrMagnitude > 0f) {
					_waterLoopTimer = _waterLoopInterval;
					if (_movementVector.magnitude > 1f) {
						_effectAudioSource.PlayOneShot(_waterLoopHeavySounds[Random.Range(0, _waterLoopHeavySounds.Length)], 0.3f);
					}
					else {
						_effectAudioSource.PlayOneShot(_waterLoopLightSounds[Random.Range(0, _waterLoopLightSounds.Length)], 0.2f * _movementVector.magnitude);
					}
				}

				// Submerge & boost
				if (boosting == true) {
					if (_submergeTimeTimer < 0f) {
						_submergeTimeTimer = 0f;
					}
					_submergeTimeTimer += Time.deltaTime;
					
					_lightCooldownTimer = _lightCooldown;
					_beamModel.GetComponent<MeshRenderer>().enabled = false;
					_beamParticle.Stop();
				}
				else {
					if (_submergeTimeTimer > _submergeTime) {
						_submergeTimeTimer = _submergeTime;
					}
					_submergeTimeTimer -= Time.deltaTime;
				}
				_model.localPosition = new Vector3(0f, -_submergeCurve.Evaluate(_submergeTimeTimer / _submergeTime) * 3f, 0f);

				float lerp = _lightLerpTimeTimer / _lightLerpTime;
				_lightCooldownTimer -= Time.deltaTime;
				if (_lightCooldownTimer <= 0f) {
					// Recharge hypnosis
					_hypnosisRechargeCooldownTimer -= Time.deltaTime;
					if (_hypnosisRechargeCooldownTimer <= 0f) {
						_hypnosisGas += _hypnosisGasRecharge * Time.deltaTime;
						if (_hypnosisGas >= _hypnosisGasMax) {
							_hypnosisGas = _hypnosisGasMax;
						}
					}

					if (hypnotizing == true) {
						if (_hypnosisGas > 0f) {
							_hypnosisGas -= _hypnosisGasConsumption * Time.deltaTime;
							if (_hypnosisGas < 0f) {
								_hypnosisGas = 0f;
							}
							_hypnosisRechargeCooldownTimer = _hypnosisRechargeCooldown;

							_lightLerpTimeTimer += Time.deltaTime;
							if (_lightLerpTimeTimer < 0f) {
								_lightLerpTimeTimer = 0f;
							}
							_beamModel.transform.localScale = Vector3.Lerp(_beamModel.transform.localScale, _lightSizeHypnosis, lerp);
							_beamMaterial.SetColor("_TintColor", Color.Lerp(_beamMaterial.GetColor("_TintColor"), _lightColorHypnosis, lerp));
							_beamModel.parent.localEulerAngles = Vector3.Lerp(_beamModel.parent.localEulerAngles, _lightRotationHypnosis, lerp);
							_hypnosisActive = true;
						}
					}
				}
				else {
					hypnotizing = false;
				}
				if (hypnotizing == false || _hypnosisGas <= 0f) {
					_lightLerpTimeTimer -= Time.deltaTime;
					if (_lightLerpTimeTimer > _lightLerpTime) {
						_lightLerpTimeTimer = _lightLerpTime;
					}
					_beamModel.transform.localScale = Vector3.Lerp(_lightSizeDefault, _beamModel.transform.localScale, lerp);
					_beamMaterial.SetColor("_TintColor", Color.Lerp(_lightColorDefault, _beamMaterial.GetColor("_TintColor"), lerp));
					_beamModel.parent.localEulerAngles = Vector3.Lerp(_lightRotationDefault, _beamModel.parent.localEulerAngles, lerp);
					_hypnosisActive = false;
				}
				_lampAudioSource.volume = lerp;
				CameraManager._instance.SetFoWLerp(lerp);

				UIManager._instance.UpdateHypnotizeBar(_hypnosisGas / _hypnosisGasMax);

				if (_lightCooldownTimer <= 0f && _eating == false) {
					_beamModel.GetComponent<MeshRenderer>().enabled = true;
					if (_beamParticle.isPlaying == false) {
						_beamParticle.Play();
					}
				}
			}
			// Dead
			else {
				//_killzoneCollider.enabled = false;
				_rotationInput = 1f;
				float shakeAmount = 2f;
				_movementVector = Vector3.MoveTowards(_movementVector, Vector3.zero, _acceleration);
				_deathSinkTimeTimer -= Time.deltaTime;
				if (_deathSinkTimeTimer > 0f) {
					float sinkCurveValue = _deathSubmergeCurve.Evaluate(_deathSinkTimeTimer / _deathSinkTime);
					float shakeCurveValue = _deathShakeCurve.Evaluate(_deathSinkTimeTimer / _deathSinkTime);
					_model.localEulerAngles = new Vector3(Random.Range(-shakeAmount, shakeAmount) * shakeCurveValue, 0f, Random.Range(-shakeAmount, shakeAmount) * shakeCurveValue);
					_model.localPosition = new Vector3(0f, -_deathSubmergeCurve.Evaluate(_deathSinkTimeTimer / _deathSinkTime) * 8f, 0f);
				}
				else {
					LevelManager._instance.GameOver();
				}
			}

			// Water particles
			for (int i = 0; i < _waterParticles.Length; i++) {
				ParticleSystem.EmissionModule eMod = _waterParticles[i].emission;
				float eRate = _waterParticleMaxEmission[i] * (_movementVector.magnitude / _boostSpeedMultp);
				eMod.rateOverTime = new ParticleSystem.MinMaxCurve(eRate);
				if (eRate < 1f && i == 0) {
					eMod.rateOverTime = new ParticleSystem.MinMaxCurve(1f);
				}
			}
		}
	}

	private void FixedUpdate() {
		if (GameManager._instance._gameState == GameManager.GameState.Running) {

			_rotationParent.Rotate(Vector3.up, _rotationInput * _rotationSpeed * Time.fixedDeltaTime);
			// Smoothly rotate lighthouse toward forward direction
			_model.eulerAngles = new Vector3(
				_rotationParent.eulerAngles.x,
				Mathf.SmoothDampAngle(_model.eulerAngles.y, _rotationParent.eulerAngles.y, ref _modelRotationVelocity, _modelRotationDampTime),
				_rotationParent.eulerAngles.z);

			// Move player
			_rigidbody.MovePosition(_rigidbody.position + (_movementVector * _movementSpeed * Time.fixedDeltaTime));
		}
		_rigidbody.velocity = Vector3.zero;
		_rigidbody.rotation = Quaternion.identity;
	}

	private void OnCollisionEnter(Collision collision) {
		if (collision.collider.tag == "Obstacle") {
			if (_movementVector.magnitude > 2f) {
				TakeDamage(1, collision.transform.position);
			}
		}
	}

	public void StartEating() {
		_beamModel.GetComponent<MeshRenderer>().enabled = false;
		_beamParticle.Stop();
		_eating = true;
		_controlsEnabled = false;
	}

	public void EndEating() {
		_eating = false;
		_controlsEnabled = true;
	}

	public void TakeDamage(int damage, Vector3 threatPosition) {
		if (_health > 0 && _shakeRoutine == null) {
			UIManager._instance.ShowBloodOverlay(1f);
			_movementVector = (transform.position - threatPosition).normalized * _movementVector.magnitude * 0.5f;

			_lightCooldownTimer = _lightCooldown;
			_beamModel.GetComponent<MeshRenderer>().enabled = false;
			_beamParticle.Stop();
			_health -= damage;
			UIManager._instance.UpdateHealth(_health);
			_effectAudioSource.PlayOneShot(_damageSound);
			// DEATH
			if (_health <= 0) {
				_alive = false;
				_deathSinkTimeTimer = _deathSinkTime;
				UIManager._instance.ShowHud(false);
				_effectAudioSource.PlayOneShot(_deathSound);
			}
			else {
				//if (_shakeRoutine != null) {
				//	StopCoroutine(_shakeRoutine);
				//}
				_shakeRoutine = StartCoroutine(DamageShakeRoutine());
			}
		}
	}

	private IEnumerator DamageShakeRoutine() {
		float shakeAmount = 4f;
		Vector3 startEulers = _model.localEulerAngles;
		for (float i = 0f; i < 0.15f; i += Time.deltaTime) {
			_model.localEulerAngles = startEulers + new Vector3(Random.Range(-shakeAmount, shakeAmount), 0f, Random.Range(-shakeAmount, shakeAmount));
			yield return null;
		}
		_model.localEulerAngles = startEulers;
		_shakeRoutine = null;
	}

	public IEnumerator CinematicActivateLightRoutine(bool b) {
		float animationTime = 0.5f;

		_beamModel.transform.localScale = _lightSizeDefault;
		_beamModel.GetComponent<MeshRenderer>().enabled = b;
		if (b == true) {
			_beamParticle.Play();
		}
		else {
			_beamParticle.Stop();
		}

		for (float i = 0f; i < animationTime; i += Time.unscaledDeltaTime) {
			float lerp = _submergeCurve.Evaluate(i / animationTime);
			if (b == true) {
				_beamMaterial.SetColor("_TintColor", Color.Lerp(Color.clear, _lightColorDefault, lerp));
			}
			else {
				_beamMaterial.SetColor("_TintColor", Color.Lerp(_lightColorDefault, Color.clear, lerp));
			}
			yield return null;
		}
	}

	public void ChangeBeamMaterial(int index) {
		_beamModel.GetComponent<MeshRenderer>().sharedMaterial = _beamMaterials[index];
	}
}
