﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraManager : MonoBehaviour {

	public static CameraManager _instance;

	public Transform _followTarget;
	public Vector3 _followPositionOffset;
	public Vector3 _followLookOffset;

	public Vector3 _orbitFollowPositionOffset;
	public Vector3 _orbitFollowLookOffset;

	public MonoBehaviour[] _postProcessingComponents;

	public Transform _audioListenerTransform;
	public AudioSource _effectAudioSource;
	public AudioSource _ambientAudioSource;

	[Header("Bad touch!")]
	public Camera _camera;
	public float _gameFow = 75f;
	public float _gameFow2 = 70f;
	public float _animationFow = 60f;
	//public Vector3 _lerpPosition;
	public Vector3 _lerpLook;
	public bool _cameraDetached = false;

	private Transform _detachTarget;

	void Awake() {
		if (_instance == null) {
			_instance = this;
			_cameraDetached = false;
			_camera = GetComponent<Camera>();
			_camera.fieldOfView = _animationFow;
			_lerpLook = _orbitFollowLookOffset;
			DontDestroyOnLoad(gameObject);
		}
		else {
			Destroy(gameObject);
		}
	}

	public void StartManager() {
		_followTarget = Player._instance.transform.GetChild(0);
		_detachTarget = DetachTarget._instance.transform;
		_detachTarget.position = transform.position;
		_detachTarget.rotation = transform.rotation;
		_camera.fieldOfView = _animationFow;
		_lerpLook = _orbitFollowLookOffset;
	}
	
	void LateUpdate() {
		if (GameManager._instance._gameState == GameManager.GameState.MainMenu) {
			// Handled by cameraOrbit
			transform.position = CameraOrbit._instance._child.position;
			transform.LookAt(_followTarget.position + _lerpLook);
		}
		else {
			if (_cameraDetached == false) {
				transform.position = _followTarget.position - (_followTarget.right * _followPositionOffset.x) -
								(_followTarget.up * _followPositionOffset.y) - (_followTarget.forward * _followPositionOffset.z);
				transform.LookAt(_followTarget.position + _followLookOffset);
			}
			else {
				if (Input.GetKeyDown(KeyCode.R)) {
					_detachTarget.position = _followTarget.position - (_followTarget.right * _followPositionOffset.x) -
								(_followTarget.up * _followPositionOffset.y) - (_followTarget.forward * _followPositionOffset.z);
					_detachTarget.LookAt(_followTarget.position + _followLookOffset);
				}
				_detachTarget.rotation = Quaternion.Euler(_detachTarget.eulerAngles + (new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f) * Time.unscaledDeltaTime * 20f));
				float multp = 1f;
				if (Input.GetKey(KeyCode.LeftShift)) {
					multp = 3f;
				}
				_detachTarget.position += _detachTarget.right * Input.GetAxisRaw("Horizontal") * Time.unscaledDeltaTime * 5f * multp;
				_detachTarget.position += _detachTarget.forward * Input.GetAxisRaw("Vertical") * Time.unscaledDeltaTime * 5f * multp;
				_detachTarget.position += Vector3.up * Input.GetAxisRaw("Vertical2") * Time.unscaledDeltaTime * 5f * multp;

				transform.position = _detachTarget.position;
				transform.rotation = _detachTarget.rotation;
			}
		}
		_audioListenerTransform.position = Player._instance.transform.position;
	}

	public void DetachCamera() {
		_cameraDetached = !_cameraDetached;
		if (_cameraDetached == true) {
			Time.timeScale = 0f;
			UIManager._instance.ShowHud(false);
			GameManager._instance._gameState = GameManager.GameState.Detatched;
		}
		else {
			Time.timeScale = 1f;
			UIManager._instance.ShowHud(true);
			GameManager._instance._gameState = GameManager.GameState.Running;
		}
	}

	public void SetFoWLerp(float lerp) {
		_camera.fieldOfView = Mathf.Lerp(_gameFow, _gameFow2, lerp);
	}

	public void SetPostProcessing(bool b) {
		for (int i = 0; i < _postProcessingComponents.Length; i++) {
			_postProcessingComponents[i].enabled = b;
		}
	}
}
