﻿using UnityEngine;
using UnityEngine.AI;

public class LevelManager : MonoBehaviour {

	public static LevelManager _instance;

	public LayerMask _playerMask;
	public LayerMask _shipMask;
	public NavMeshSurface _navMesh;

	public NavMeshAgent _shipSmallPrefab;
	public NavMeshAgent _shipLargePrefab;

	private float _largeShipSpawnPercentage = 0f;
	private float _smallshipPercentageIncrease = 0.1f;
	private float _largeShipPercentageDecrease = 0.3f;

	[Header("Don't touch pls")]
	public bool _spawningActive = true;

	private float _levelStartTime = 105f;
	private float _levelTimer;
	public int _score;

	private float _shipSpawnDistance = 110f;
	private float[] _shipSpawnInterval = new float[] { 5f, 9f };
	private float _shipSpawnTimer = 0f;

	private void Awake() {
		_instance = this;
	}

	public void StartManager() {
		_levelTimer = _levelStartTime;
		_navMesh.BuildNavMesh();
	}

	public void StartGame() {
		_spawningActive = true;
		// Activate Player etc
	}
	
	public void GameOver() {
		GameManager._instance._gameState = GameManager.GameState.GameOver;
		if (GameManager._instance._topScore < _score) {
			GameManager._instance._topScore = _score;
		}
		GameManager._instance.ChangeCursorLock();
		UIManager._instance.ShowGameOver(true);
	}

	void Update() {
		if (GameManager._instance._gameState == GameManager.GameState.Running) {
			_levelTimer -= Time.deltaTime;
			if (Player._instance._alive == true) {
				if (_levelTimer > 0f) {
					UIManager._instance.UpdateLevelTimer(_levelTimer);
				}
				if (_levelTimer < 0f) {
					Player._instance.TakeDamage(10, Vector3.zero);
				}
			}

			if (_spawningActive == true) {
				_shipSpawnTimer -= Time.deltaTime;
				if (_shipSpawnTimer <= 0f) {
					_shipSpawnTimer = Random.Range(_shipSpawnInterval[0], _shipSpawnInterval[1]);
					Vector3 spawnPos = Quaternion.AngleAxis(Random.Range(0f, 360f), Vector3.up) * Vector3.forward * _shipSpawnDistance;
					//print(_largeShipSpawnPercentage);
					if (Random.value < _largeShipSpawnPercentage) {
						Instantiate(_shipLargePrefab, spawnPos, Quaternion.identity);
						_largeShipSpawnPercentage -= _largeShipPercentageDecrease;
					}
					else {
						Instantiate(_shipSmallPrefab, spawnPos, Quaternion.identity);
						_largeShipSpawnPercentage += _smallshipPercentageIncrease;
					}
				}
			}
		}
	}

	public void IncreaseValues(int score, int time, Vector3 position) {
		_score += score;
		UIManager._instance.UpdateScore(_score);
		UIManager._instance.CreatePopUp(position, score, false);
		if (time > 0) {
			_levelTimer += time;
			UIManager._instance.CreatePopUp(position, time, true);
		}
	}
}
